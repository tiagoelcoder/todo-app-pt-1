import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 } from 'uuid';
class App extends Component {
  state = {
    todos: todosList,
    nextTodo: "",
  }; 

  checkTodo =(id) => (event) => { 
    const updatedTodos = this.state.todos.map(todo => {
      // if this task has the same ID as the edited task
      if (id === todo.id) {
        // use object spread to make a new object
        // whose `completed` prop has been inverted
        return {...todo, completed: !todo.completed}
      }
      return todo;
    });
    this.setState({todos: updatedTodos});
  }

  addTodo = (item) => { 
    this.setState(state => ({ 
      todos: [item,...state.todos]
    }))
  }
  removeTodo = (id) => (event) => { 
    let remainingTodos = this.state.todos.filter(todo => id !== todo.id)
    this.setState({todos: remainingTodos});
  }

  clearTodos =() => {  
   const pendingTodos = this.state.todos.filter(todo=> !todo.completed )
    this.setState({todos: pendingTodos});
  }
  

   handleSubmit = (event) => {  
     event.preventDefault()
     let newItem =  {
      userId: 1,
      id: v4(),
      title: this.state.nextTodo,
      completed: false
    }
    this.addTodo(newItem)
    this.setState({nextTodo: ""})

   }

  handleChange =(event) => {
    event.preventDefault()
    event.persist()
    this.setState({ nextTodo: event.target.value})
  }
 
  render() {
    const todoCount = this.state.todos.length
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form  onSubmit ={this.handleSubmit} >
          <input 
          onChange={this.handleChange}
          value={this.state.nextTodo} 
          className="new-todo" 
          placeholder="What needs to be done?" autoFocus />
          </form>
        </header>
        <TodoList removeTodo = {this.removeTodo} checkTodo ={this.checkTodo} todos={this.state.todos} />
        <footer className="footer">
          <span className="todo-count">
            <strong>{todoCount}</strong> item(s) left
          </span>
          <button onClick ={this.clearTodos} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

const TodoItem = ({completed,title, checkTodo, id, removeTodo }) => {

    return (
      <li className={completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" onChange={checkTodo(id)} checked={completed} />
          <label>{title}</label>
          <button className="destroy" onClick ={removeTodo(id)} />
        </div>
      </li>
    );
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
             id ={todo.id} 
             key ={todo.id} 
             title={todo.title} 
             completed={todo.completed} 
            checkTodo={this.props.checkTodo}
            removeTodo={this.props.removeTodo} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
